/*
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * Version 2, December 2004 
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 * 0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.cs.vgonsalv;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Vic
 */
public class ChatServer implements Runnable, AutoCloseable {

    private static final int DEFAULT_PORT = 8675;
    private static final Logger logger = Logger.getLogger(ChatServer.class.getName());
    private static final int MAX_CLIENTS = 64;
    private static final int READ_TIMEOUT = 1000 * 60  * 5;

    private final int port;
    private boolean run;
    private final List<ClientHandler> clients;
    private final ThreadPoolExecutor pool;

    public ChatServer() {
        this(DEFAULT_PORT);
    }

    public ChatServer(int port) {
        this.port = port;
        this.run = true;
        this.clients = Collections.synchronizedList(new ArrayList<>());
        this.pool = new ThreadPoolExecutor(2, MAX_CLIENTS, 2, TimeUnit.MINUTES,
                new ArrayBlockingQueue<>(MAX_CLIENTS), (Runnable r, ThreadPoolExecutor executor) -> {
                    try (ClientHandler ch = (ClientHandler) r) {
                        ch.writeError(null, "Server full");
                    } catch (IOException ex) {
                        logger.log(Level.SEVERE, null, ex);
                    }
                });
    }

    private void broadcast(ClientHandler src, String message) {
        logger.log(Level.FINE, "broadcasting {0}", message);
        synchronized (clients) {
            clients.stream().forEach((c) -> {
                try {
                    c.writeMessage(src, message);
                } catch (IOException ex) {
                    logger.log(Level.SEVERE, null, ex);
                    c.close();
                }
            });
        }
    }

    @Override
    public void run() {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            while (run) {
                Socket clientSocket = serverSocket.accept();
                logger.log(Level.FINE, "Client connected from {0}", clientSocket.getRemoteSocketAddress());
                clientSocket.setTcpNoDelay(true);
                clientSocket.setSoTimeout(READ_TIMEOUT);
                ClientHandler c = new ClientHandler(clientSocket);
                clients.add(c);
                pool.submit(c);
            }
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Server Encountered Error", ex);
        } finally {
            close();
        }
    }

    @Override
    public void close() {
        synchronized (clients) {
            clients.stream().forEach((c) -> {
                c.close();
            });
            run = false;
        }
    }

    public static void main(String[] args) {
        int port = DEFAULT_PORT;
        if (args.length > 0) {
            try {
                port = Integer.parseInt(args[0]);
            } catch (NumberFormatException ex) {
            }
        }
        try (ChatServer server = new ChatServer(port)) {
            logger.log(Level.INFO, "ChatServer staring on port {0}", server.port);
            server.run();
        }
    }
    enum MessageType{
        ERROR("error"),
        MESSAGE("message"),
        NAME("name"),
        INVALID("");
        private static final Map<String,MessageType> messages = Arrays.stream(values())
                .filter(v->!v.stringVal.isEmpty())
                .collect(Collectors.toMap(v->v.stringVal, v->v));
        final String stringVal;

        private MessageType(String stringVal) {
            this.stringVal = stringVal;
        }

        @Override
        public String toString() {
            return stringVal;
        }
        public static MessageType parse(String s){
            return messages.getOrDefault(s, INVALID);
        }
    }
    class ClientHandler implements Runnable, AutoCloseable {
        private final Socket client;
        private final Writer toClient;
        private final BufferedReader fromClient;
        private final Object writeLock;
        private String clientName;

        public ClientHandler(Socket client) throws IOException {
            this.client = client;
            this.writeLock = new Object();
            this.toClient = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
            this.fromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
        }

        public void writeMessage(ClientHandler src, String message) throws IOException {
            if (src != this) {
                write(MessageType.MESSAGE, message);
            }
        }

        public void writeError(ClientHandler src, String message) throws IOException {
            if (src != this) {
                write(MessageType.ERROR, message);
            }
        }

        private void write(MessageType type, String s) throws IOException {
            synchronized (writeLock) {
                toClient.write(type.toString());
                toClient.write("\n");
                toClient.write(""+s.length());
                toClient.write("\n");
                toClient.write(s);
                toClient.flush();
            }
            logger.log(Level.FINER, "{0} was sent {1}", new Object[]{clientName, s});
        }

        @Override
        public void run() {
            try {
                String line;
                while ((line = fromClient.readLine()) != null) {
                    MessageType type = MessageType.parse(line);
                    int length;
                    if ((line = fromClient.readLine()) != null) {
                        try {
                            length = Integer.parseInt(line);
                        } catch (ArithmeticException ex) {
                            writeError(null, "invlid length field: " + line);
                            close();
                            return;
                        }
                    } else {
                        writeError(null, "message length expected");
                        close();
                        return;
                    }
                    char[] bodyArray = new char[length];
                    fromClient.read(bodyArray, 0, length);

                    String body = new String(bodyArray);
                    switch (type) {
                        case ERROR:
                            logger.log(Level.WARNING, "{0}", body);
                            client.close();
                            break;
                        case MESSAGE:
                            String message = getClientName() + ": " + body;
                            ChatServer.this.broadcast(this, message);
                            break;
                        case NAME:
                            clientName = body;
                            break;
                        case INVALID:
                            writeError(null, String.format("Unknown message type: %s", type));
                            return;
                    }
                }
            } catch (SocketTimeoutException timeOutException) {
                try {
                    writeError(null, "Timeout");
                } catch (IOException ioException) {
                    logger.log(Level.SEVERE, null, ioException);
                }
            } catch (IOException ex) {
                if ("Connection reset".equals(ex.getMessage())) {
                    this.close();
                    logger.log(Level.INFO, "{0} disconected", getClientName());
                    ChatServer.this.broadcast(this, getClientName() + " disconnected");
                } else {
                    try {
                        writeError(null, "A server Error has occured\n");
                    } catch (IOException ex1) {
                        logger.log(Level.SEVERE, null, ex1);
                    }
                    logger.log(Level.SEVERE, "Client Handler Error", ex);
                    close();
                }
            } finally {
                this.close();
            }
        }

        public String getClientName() {
            return clientName == null ? "" : clientName;
        }

        @Override
        public void close() {
            clients.remove(this);
            try {
                toClient.close();
            } catch (IOException e) {
                logger.log(Level.SEVERE, "Failed to client output stream", e);
            }
            try {
                fromClient.close();
            } catch (IOException e) {
                logger.log(Level.SEVERE, "Failed to close client input stream", e);
            }
            try {
                client.close();
            } catch (IOException e) {
                logger.log(Level.SEVERE, "Failed to closes client socket", e);
            }
        }

    }

}
