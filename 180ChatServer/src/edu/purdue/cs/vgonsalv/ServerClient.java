/*
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * Version 2, December 2004 
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 * 0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.cs.vgonsalv;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Vic
 */
public class ServerClient {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter host address");
        String host = input.nextLine();
        System.out.println("Enter port number");
        int port = input.nextInt();
        input.nextLine();
        Socket server = new Socket(host, port);
        server.setTcpNoDelay(true);
        new Thread(() -> {
            try (BufferedReader fromServer = new BufferedReader(new InputStreamReader(server.getInputStream()))) {
                String line;
                while ((line = fromServer.readLine()) != null) {
                    String type = line;
                    line = fromServer.readLine();
                    int length = Integer.parseInt(line);
                    char[] buffer = new char[length];
                    fromServer.read(buffer, 0, length);
                    switch (type) {
                        case "message":
                            System.out.print(buffer);
                            System.out.flush();
                            break;
                        case "error":
                            System.err.print(buffer);
                            System.err.flush();
                            break;
                    }
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }finally{
                try {
                    server.close();
                } catch (IOException ex) {
                    Logger.getLogger(ServerClient.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }).start();
        try (BufferedWriter toServer = new BufferedWriter(new OutputStreamWriter(server.getOutputStream()))) {
            System.out.println("Enter name");
            String name = input.nextLine().trim() + "\n";
            toServer.write("name\n");
            toServer.write("" + name.length() + "\n");
            toServer.write(name);
            toServer.flush();
            while (true) {
                String line = input.nextLine().trim() + "\n";
                toServer.write("message\n");
                toServer.write("" + line.length() + "\n");
                toServer.write(line);
                toServer.flush();
            }
        }finally{
            server.close();
        }

    }

}
